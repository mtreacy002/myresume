# Maya Treacy's Portfolio

Credits:

- resume template: https://www.styleshout.com/free-templates/ceevee/
- email processing service: https://kontactr.com
- header background image: Pixabay (https://www.pexels.com/photo/scenic-view-of-dramatic-sky-during-winter-258112/)
- "Healthy Resto Journal" photo: TUBARONES PHOTOGRAPHY (https://www.pexels.com/photo/veggie-dish-on-white-ceramic-bowl-3026013/)
- "SMARTerBackEnd" photo: Pixabay (https://www.pexels.com/photo/ball-bright-close-up-clouds-207489/)
- "SMARTerFrontEnd" photo: Burak K (https://www.pexels.com/photo/light-45072/)
- "Monash Zoo Map" photo: Pixabay (https://www.pexels.com/photo/animal-big-cat-jungle-safari-206622/)
- "Healthy Plant" photo: Mike (https://www.pexels.com/photo/beautiful-bloom-blooming-blossom-109813/)
- "Safe Crossing" photo: Alex Powell (https://www.pexels.com/photo/person-walking-on-pedestrian-lane-1769387/)

- "Rockets 1 & 2" photo: Pixabay (https://www.pexels.com/photo/flight-sky-earth-space-2166/)

- "Rockets 3" photo: Pixabay (https://www.pexels.com/photo/sea-flight-sky-earth-2163/)
